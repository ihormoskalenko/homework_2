﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace task_2
{
    class Program
    {
        public delegate int SomeDelegate();
        public delegate double AvarageValue(SomeDelegate[] DelegatesArray);


        static void Main(string[] args)
        {
            SomeDelegate[] myDelegatesArray = new SomeDelegate[new Random().Next(1, 20)];

            for (int i = 0; i < myDelegatesArray.Length; i++)
                myDelegatesArray[i] = () => new Random().Next(0, 200);

            AvarageValue CalculateAvarageValue = delegate (SomeDelegate[] arr)
            {
                double valueSum = 0;
                for (int i = 0; i < arr.Length; i++)
                    valueSum += arr[i]();

                return valueSum / arr.Length;
            };

            


            Console.WriteLine(CalculateAvarageValue(myDelegatesArray));
        }
    }
}
