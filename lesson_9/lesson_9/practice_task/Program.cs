﻿using System;

namespace practice_task
{

    public delegate bool FilterCondition(int i);

    public static class ExtensionMethodForArray
    {
        public static int[] Filter(this int [] array , FilterCondition condition )
        {
            int[] buff = new int[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (condition(array[i]))
                {
                    Array.Resize(ref buff, buff.Length + 1);
                    buff[buff.Length - 1] = array[i];
                }

            }
            return buff;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 3, 4, 5, 6, -1, -3, -4, -6, -7, 4 };
            
            var OnlyNegativeElements = array.Filter(element => element < 0);

            for (int i = 0; i < OnlyNegativeElements.Length; i++)
            {
                Console.WriteLine(OnlyNegativeElements[i]);
            }
        }
    }
}
