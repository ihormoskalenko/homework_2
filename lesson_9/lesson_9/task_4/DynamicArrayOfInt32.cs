﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace DynamicArrayOfInt
{
    public class DynamicArrayOfInt32
    {
        private int[] _sourceArray;
        private int _arraySize, _initedElements;

        public DynamicArrayOfInt32(int arraySize)
        {
            _arraySize = arraySize;
            _initedElements = 0;
            _sourceArray = new int[arraySize];
        }

        public DynamicArrayOfInt32() :
            this(0)
        { }

        public DynamicArrayOfInt32(int[] values)
        {
            _sourceArray = new int[values.Length];
            for (int i = 0; i < values.Length; i++)
                _sourceArray[_arraySize++] = values[i];
            _initedElements = _arraySize;
        }

        public void AutoFill(int numberOfElements)
        {

            if (_arraySize != numberOfElements)
                Resize(numberOfElements);
            for (int i = 0; i < _arraySize; i++)
            {
                _sourceArray[i] = new Random().Next(-100, 100);
            }
        }
        public void Add(int value)
        {
            if (_initedElements < _arraySize)
                _sourceArray[_initedElements++] = value;
            else
            {
                this.Resize(_arraySize+1);
                _sourceArray[_initedElements++] = value;
            }
        }

        private void Resize(int newSize)
        {
            int[] buf = _sourceArray;
            _sourceArray = new int[_arraySize + newSize];
            for (int i = 0; i < _arraySize; i++)
            {
                _sourceArray[i] = buf[i];
            }
            _arraySize = newSize;
        }

        public int this[int i]
        {
            get { return _sourceArray[i]; }
            set { _sourceArray[i] = value; }
        }

        public int Length
        {
            get { return _arraySize; }
        }

    }
}
