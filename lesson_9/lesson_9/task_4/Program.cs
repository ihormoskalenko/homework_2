﻿using System;
using DynamicArrayOfInt;

namespace task_4
{


    public delegate void IntAction(int someValue);

    public static class ExtensionMEthodForDyanmicArray
    {
        public static void ForEach(this DynamicArrayOfInt32 list, IntAction action)
        {
            for (int i = 0; i < list.Length; i++)
                action(list[i]);
        }
    }
    class Program
    {


        static void Main(string[] args)
        {
            DynamicArrayOfInt32 MyArray = new DynamicArrayOfInt32(10);

            for (int i = 0; i < MyArray.Length; i++)
            {
                MyArray.Add(new Random().Next(-100, 100));
            }

            MyArray.ForEach(x => Console.WriteLine(x));

        }
    }
}
