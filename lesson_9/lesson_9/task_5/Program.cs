﻿using System;
using DynamicArrayOfInt;

namespace task_5
{

    public delegate double BinaryExpression(double param1 , double param2);

    public static class DynamicArrayExtension
    {
        public static double Accumulate(this DynamicArrayOfInt32 list, BinaryExpression accumulator)
        {
            double buf = list[0];
            for (int i = 1; i < list.Length; i++)
            {
                buf = accumulator(buf, list[i]);
            }
            return buf;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DynamicArrayOfInt32 MyArray = new DynamicArrayOfInt32(new int[] {1,2,3,4,5,6,7,8,9});
            //MyArray.AutoFill(100);

            Console.WriteLine(MyArray.Accumulate((x,y) => x*y));

        }
    }
}
