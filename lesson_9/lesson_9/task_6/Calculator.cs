﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task_6
{
    class Calculator
    {

        private delegate double DivideByZeroErrorHandler(double firstValue , double secondValue);

        private DivideByZeroErrorHandler DivideByZero;

        public Calculator()
        {
            DivideByZero = delegate (double firstValue , double secondValue)
            {
                if (secondValue == 0)
                {
                    Console.WriteLine($"Cannot divide {firstValue} by {secondValue}. Divide by zero not supported.");
                    return double.NaN;
                }
                else
                    return firstValue/secondValue;
            };
        }

        //public 

        public double Add(double firstValue, double secondValue)
        {
            return firstValue + secondValue;
        }

        public double Sub(double firstValue, double secondValue)
        {
            return firstValue - secondValue;
        }

        public double Mul(double firstValue, double secondValue)
        {
            return firstValue * secondValue;
        }

        public double Div(double firstValue, double secondValue)
        {
            
            return DivideByZero(firstValue, secondValue);

        }

        


    }
}
