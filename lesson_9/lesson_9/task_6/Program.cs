﻿using System;

namespace task_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator MyCalculator = new Calculator();
            Console.WriteLine(MyCalculator.Add(67.5, 1396.04));
            Console.WriteLine(MyCalculator.Div(22.4, 82));
            Console.WriteLine(MyCalculator.Div(13.5, 0));
            Console.WriteLine(MyCalculator.Sub(0.5, 46.4));
            Console.WriteLine(MyCalculator.Mul(35, 1765));

        }
    }
}
