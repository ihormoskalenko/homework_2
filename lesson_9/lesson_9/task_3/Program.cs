﻿using System;
using DynamicArrayOfInt;

namespace task_3
{

    public delegate bool FilterDelegate(int value);

    public static class ExtensionFilterMethod
    {
        public static DynamicArrayOfInt32 Filter(this DynamicArrayOfInt32 array, FilterDelegate @delegate)
        {
            DynamicArrayOfInt32 buff = new DynamicArrayOfInt32(0);

            for (int i = 0; i < array.Length; i++)
            {
                if (@delegate(array[i]))
                {
                    buff.Add(array[i]);
                }
                    
            }
            return buff;

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DynamicArrayOfInt32 array = new DynamicArrayOfInt32(new int [] { 1, 2, 3, 4, 5, 6, -1, -3, -4, -6, -7, 4 });

            var OnlyNegativeElements = array.Filter(element => element < 0);

            for (int i = 0; i < OnlyNegativeElements.Length; i++)
            {
                Console.WriteLine(OnlyNegativeElements[i]);
            }

        }
    }
}
