﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

namespace task_1
{
    class Program
    {

        public static Func<double, double, double> Add = (numberA, numberB) => numberA + numberB;
        public static Func<double, double, double> Div = (numberA, numberB) =>{
            if (numberB == 0)
            {
                Console.WriteLine("Диление на ноль невозможно!");
                return -1;
            }
            return numberA / numberB;
        };
        public static Func<double, double, double> Sub = (numberA, numberB) => numberA - numberB;
        public static Func<double, double, double> Mul = (numberA, numberB) => numberA * numberB;



        static void Main(string[] args)
        {

            Console.WriteLine(Div(12, 2.3));
            
        }
    }
}
