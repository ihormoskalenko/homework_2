﻿using System;

namespace task_4
{
    class Store
    {
        private Article[] _products;


        public Store()
        {
            _products = new Article[0];
        }
        public void AddProduct()
        {
            Console.WriteLine("Введите название продукта :");
            string name = Console.ReadLine();
            Console.WriteLine("Введите цену товара :");
            int price = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите месторасположения товара :");
            string store = Console.ReadLine();
            Array.Resize<Article>(ref _products, _products.Length + 1);
            _products[_products.Length - 1] = new Article(name, store, price);
        }

        public void AddProduct(string name, string store, int price)
        {
            Array.Resize<Article>(ref _products, _products.Length + 1);
            _products[_products.Length - 1] = new Article(name, store, price);
        }

        public Article this[int index]
        {
            get {
                if (index >= _products.Length || index < 0)
                    return new Article(null, null, 0);
                return _products[index]; }
        }
        public Article this[string name]
        {
            get
            {
                for (int i = 0; i < _products.Length; i++)
                {
                    if (_products[i].ProductName == name)
                    {
                        return _products[i];
                    }
                }
                return new Article(null, null, 0); ;
            }
        }
    }
}
