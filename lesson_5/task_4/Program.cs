﻿using System;
using System.Runtime.InteropServices.ComTypes;

namespace task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Store MyStore = new Store();



            MyStore.AddProduct("Cola", "Metro", 10);
            MyStore.AddProduct("Pepsi", "Silpo", 13);
            MyStore.AddProduct("Sosage", "Fora", 160);
            MyStore.AddProduct("Skirt", "Prada", 1000);
            MyStore.AddProduct("Pants", "Metro", 500);
            MyStore.AddProduct("Socks", "dfd", 56);
            MyStore.AddProduct("Fanta", "Silpo", 21);
            MyStore.AddProduct("Soda", "Metro", 12);

            MyStore.AddProduct();

            Console.WriteLine("Введите название товара для поиска :");
            string buff = Console.ReadLine();
            
            MyStore[buff].GetInfo();
            MyStore["Pants"].GetInfo();
            MyStore["Pant"].GetInfo();
            MyStore[3].GetInfo();
            MyStore["Fanta"].GetInfo();
            MyStore[4].GetInfo();
            MyStore[10].GetInfo();
        }
    }
}
