﻿using System;

namespace task_4
{
    class Article
    {
        private string _productName;
        private string _productStore;
        private int _productPrice;


        public Article()
        {
            
        }
        public Article(string productName, string productStore, int productPrice)
        {
            _productName = productName;
            _productPrice = productPrice;
            _productStore = productStore;
        }

        public string ProductName
        {
            get { return _productName; }
        }

        public string ProductStore
        {
            get { return _productStore; }
        }
        public int ProductPrice
        {
            get { return _productPrice; }
        }

        public void GetInfo()
        {
            if (this.ProductName == null)
            {
                Console.WriteLine("__________________________________");
                Console.WriteLine("Товар отсутвеут!");
                Console.WriteLine();
                return;
            }
            Console.WriteLine("__________________________________");
            Console.WriteLine("Название : {0}", ProductName);
            Console.WriteLine("Цена : {0}", ProductPrice);
            Console.WriteLine("Место продахи : {0}", ProductStore);
            Console.WriteLine();
        }

    }
}
