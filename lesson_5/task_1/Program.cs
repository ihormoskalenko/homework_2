﻿using System;

namespace task_1
{

    class DynamicArrayOfInt
    {
        private const int DefaultArraySize = 4;
        private int[] _sourceArray;

        public int Length { get; set; }
        public int TotalSize { get; set; }

        public DynamicArrayOfInt() 
            :this (DefaultArraySize)
        {
        }

        public DynamicArrayOfInt(int initialSize)
        {
            Length = 0;
            TotalSize = initialSize;
            _sourceArray = new int[initialSize];
        }

        public void Add(int newElement)
        {
            if (Length <TotalSize)
            {
                _sourceArray[Length++] = newElement;
                Console.WriteLine("Succesfull add!");
            }
            else 
            {
                ChangeSize(TotalSize + 1);
                _sourceArray[Length++] = newElement;
            }
        }


        private void ChangeSize(int newSize)
        {
            int[] buff = _sourceArray;
            _sourceArray = new int[newSize];
            for (int i = 0; i < Length; i++)
                _sourceArray[i] = buff[i]; 
            TotalSize = newSize;
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            var NewDynamicArray = new DynamicArrayOfInt();

            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
            NewDynamicArray.Add(2);
        }
    }
}
