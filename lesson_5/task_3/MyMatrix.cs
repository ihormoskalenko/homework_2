﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task_3
{
    class MyMatrix
    {
        private int _rows;
        private int _columns;
        private int _startGenerateColumn, _startGenerateRow;
        private int[,] _array;

        public MyMatrix(int Rows, int Columns)
        {
            _rows = Rows;
            _columns = Columns;
            _startGenerateColumn = _startGenerateRow = 0;
            _array = new int[_columns, _rows];
        }

        public int Rows
        {
            get { return _rows; }
        }

        public int Columns
        {
            get { return _columns; }
        }

        public int this[int row, int column]
        {
            get { return _array[row, column]; }
            set { _array[row, column] = value; }
        }

        public void Add(int column, int row, int value)
        {
            if (column >= _columns || row >= _rows)
            {
                Console.WriteLine("Елемента с такими индексами не существует!");
                return;
            }
            _array[column, row] = value;
        }

        public void ChangeSize(int columns, int rows)
        {

            int[,] BuffArray = _array;
            _array = new int[columns, rows];
            for (int i = 0; i < _columns; i++)
            {
                for (int j = 0; j < _rows; j++)
                {
                    _array[i, j] = BuffArray[i, j];
                }
            }
            _startGenerateColumn = _columns;
            _startGenerateRow = _rows;
            _columns = columns;
            _rows = rows;
        }

        public void Print()
        {
            for (int i = 0; i < _columns; i++)
            {
                for (int j = 0; j < _rows; j++)
                {
                    Console.Write(_array[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public void GenerateMatrix(bool FullGenerate = false)
        {
            for (int i = 0; i < _columns; i++)
            {
                for (int j = 0; j < _rows; j++)
                {
                    if (i >= _startGenerateColumn || j >= _startGenerateRow || FullGenerate)
                        _array[i, j] = new Random().Next(-200, 200);
                }
            }
        }
    }
}
