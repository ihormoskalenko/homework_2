﻿using System;

namespace task_3
{
    class Program
    {
        static void Main(string[] args)
        {

            MyMatrix NewMyMatrix = new MyMatrix(10, 10);

            NewMyMatrix.Print();
            NewMyMatrix.GenerateMatrix();
            NewMyMatrix.Print();

            NewMyMatrix.Add(11, 11, 34);
            NewMyMatrix.ChangeSize(15 , 15);
            NewMyMatrix.Add(11, 11, 34);

            //NewMyMatrix.GenerateMatrix();
            NewMyMatrix.Print();

            NewMyMatrix.GenerateMatrix(true);
            NewMyMatrix.Print();
        }
    }
}
