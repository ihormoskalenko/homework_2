﻿using System;

namespace task_2
{
    class Program
    {
        static void Main(string[] args)
        {

            int arraySize = new Random().Next(0,500);
            int[] randomArray = new int[arraySize];

            int arrayMin = 0;
            int arrayMax = 0;
            int arraySum = 0;
            int[] oddNumbers = new int[0];


            for (int i = 0; i < arraySize; i++)
            {
                randomArray[i] = new Random().Next(-500, 500);
            }

            for (int i = 0; i < arraySize; i++)
            {
                if (randomArray[i] < arrayMin)
                    arrayMin = randomArray[i];
                if (randomArray[i] > arrayMax)
                    arrayMax = randomArray[i];
                arraySum += randomArray[i];
                if (randomArray[i] % 2 != 0)
                {
                    Array.Resize<int>(ref oddNumbers, oddNumbers.Length + 1);
                    oddNumbers[oddNumbers.Length - 1] = randomArray[i];
                }



            }

            Console.WriteLine("Максимальное значение массива : {0}", arrayMax);
            Console.WriteLine("Минимальное значение массива : {0}", arrayMin);
            Console.WriteLine("Сумма всех елементов массива : {0}", arraySum);
            Console.WriteLine("Среднее арифметическое всех елементов массива : {0}", (double)arraySum/(double)arraySize);
            Console.WriteLine("Нечетные елементы массива :");
            for (int i = 0; i < oddNumbers.Length; i++)
            {
                Console.Write(oddNumbers[i] + "  ");
                if (i % 10 == 0 && i!=0)
                    Console.WriteLine();
            }

        }
    }
}
