﻿using System;

namespace task_1
{
    abstract class AbstractHandler
    {
        public abstract void Open();

        public abstract void Create();

        public abstract void Chenge();

        public abstract void Save();

    }

    class XMLHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Open XML file!");
        }
        public override void Create()
        {
            Console.WriteLine("Create XML file!");
        }
        public override void Chenge()
        {
            Console.WriteLine("Chenge XML file!");
        }
        public override void Save()
        {
            Console.WriteLine("Save XML file!");
        }

    }

    class TXTHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Open TXT file!");
        }
        public override void Create()
        {
            Console.WriteLine("Create TXT file!");
        }
        public override void Chenge()
        {
            Console.WriteLine("Chenge TXT file!");
        }
        public override void Save()
        {
            Console.WriteLine("Save TXT file!");

        }
    }

    class DOCHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Open DOC file!");
        }
        public override void Create()
        {
            Console.WriteLine("Create DOC file!");
        }
        public override void Chenge()
        {
            Console.WriteLine("Chenge DOC file!");
        }
        public override void Save()
        {
            Console.WriteLine("Save DOC file!");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            AbstractHandler C = new TXTHandler();

            C.Create();
            C.Open();
            C.Chenge();
            C.Save();
        }
    }
}
