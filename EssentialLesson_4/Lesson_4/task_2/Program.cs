﻿using System;

namespace task_2
{
    interface IPlayable
    {
        void Play();
        void Pause();
        void Stop();
    }

    interface IRecordable
    {
        void Record();
        void Pause();
        void Stop();

    }
    class Player : IRecordable, IPlayable
    {

        bool IsPlay = false;
        bool IsRec = false;
        public void Play()
        {
            if (!IsRec && !IsPlay)
            {
                IsPlay = true;
                Console.WriteLine("Start play!");
            }
        }
        public void Pause()
        {
            if (IsPlay)
            {
                IsPlay = false;
                Console.WriteLine("Pause play!");
            }
            if (IsRec)
            {
                IsRec = false;
                Console.WriteLine("Pause record!");
            }
        }
        public void Stop()
        {
            if (IsPlay)
            {
                IsPlay = false;
                Console.WriteLine("Stop play!");
            }
            if (IsRec)
            {
                IsRec = false;
                Console.WriteLine("Stop record!");
            }
        }
        public void Record()
        {
            if (!IsRec && !IsPlay)
            {
                IsRec = true;
                Console.WriteLine("Start record!");
            }
        
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Player MyPlayer = new Player();

            Console.WriteLine(" Press  '1' for play , '2' for record , '3' for pause ,  '4' for stop and 'x' for exet from programm !");
            bool status = true;
            while (status)
            {

                ConsoleKey key = Console.ReadKey(true).Key;



                switch (key)
                {
                    case ConsoleKey.D1:
                        MyPlayer.Play();
                        break;
                    case ConsoleKey.D2:
                        MyPlayer.Record();
                        break;
                    case ConsoleKey.D3:
                        MyPlayer.Pause();
                        break;
                    case ConsoleKey.D4:
                        MyPlayer.Stop();
                        break;
                    case ConsoleKey.X:
                        status = false;
                        break;

                }
            }

        }
    }
}
